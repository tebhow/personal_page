class AddColumnsToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :title, :string
    add_column :users, :born_on, :date
    add_column :users, :address, :text
    add_column :users, :zip_code, :string
    add_column :users, :phone, :string
    add_column :users, :say_hello, :text
  end
end
