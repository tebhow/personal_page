Rails.application.routes.draw do

  #get 'sessions/new'

  #get 'users/show'
  #get 'users/edit'
  #get 'users/update'

  # ------------------------------------------------------------ #

  root    'single_page#index'					                  # Root_Path

  #get     '/admin',           to: 'admin_page#index'    # admin_path admin login
  #get     '/admin/about',     to: 'users#about'    # admin_about_path
  #get     '/admin/about/edit',     to: 'users#edit'    # admin_about_path
  #patch     '/admin/about/update',     to: 'users#update'    # admin_about_path
  #put     '/admin/about/update',     to: 'users#update'    # admin_about_path  

  get     '/admin',           to: 'sessions#new'        # admin_path start session login
  post    '/admin',           to: 'sessions#create'     # admin_path create session login
  delete  '/logout',          to: 'sessions#destroy'    # login_path destroy session (logout)

  scope "/admin" do
    resources :users, :only => [:show, :edit, :update]
  end
  # users_path using resouces method to obtain a full suite of RESTful routes automatically

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

end
