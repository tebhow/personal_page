require 'test_helper'

class UserTest < ActiveSupport::TestCase
  
  def setup
    @user = User.new(name: "example", fullname: "Example Example", email: "user@example.com",
                     password: "foobar78", password_confirmation: "foobar78")
  end

  test "should be valid" do
    assert @user.valid?
  end

# Test column should be present
  test "name should be present" do
    @user.name = "     "
    assert_not @user.valid?
  end

  test "fullname should be present" do
    @user.fullname = "     "
    assert_not @user.valid?
  end

  test "email should be present" do
    @user.email = "     "
    assert_not @user.valid?
  end

# Test column should not be too long
  test "name should not be too long" do
    @user.name = "a" * 51
    assert_not @user.valid?
  end

  #test "fullname should not be too long" do
  #  @user.fullname = "a" * 244
  #  assert_not @user.valid?
  #end

  test "email should not be too long" do
    @user.email = "a" * 244 + "@example.com"
    assert_not @user.valid?
  end

# Test email validation
  test "email validation should accept valid addresses" do
    valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn]
    valid_addresses.each do |valid_address|
      @user.email = valid_address
      assert @user.valid?, "#{valid_address.inspect} should be valid"
    end
  end

# Test email should be saved as lower-case
  test "email addresses should be saved as lower-case" do
    mixed_case_email = "Foo@ExAMPle.CoM"
    @user.email = mixed_case_email
    @user.save
    assert_equal mixed_case_email.downcase, @user.reload.email
  end

# Test Password present
  test "password should be present (nonblank)" do
    @user.password = @user.password_confirmation = " " * 8
    assert_not @user.valid?
  end

  test "password should have a minimum length" do
    @user.password = @user.password_confirmation = "a" * 7
    assert_not @user.valid?
  end

end
