require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:anugrah)
  end

  test "unsuccessful edit" do
    get edit_user_path(@user)
    assert_template 'admin/users/edit'
    patch user_path(@user), params: { user: { name:  "",
                                              }

    assert_template 'admin/users/edit'
  end

end
