module ApplicationHelper
	# Returns the full title on a per-page basis
	def full_title(page_title)
		base_title = "Personal App"
		if page_title.empty?
			base_title
		else 
			page_title + " | " + base_title
		end
	end

	# Returns the Admin title on a per-page basis
	def admin_title(page_title)
		base_title = "Admin Page"
		if page_title.empty?
			base_title
		else 
			page_title + " | " + base_title
		end
	end

end
