class AdminPageController < ApplicationController

  # index and about can be opened if admin logged_in
  before_action :logged_in_user, only: [:index, :about]

  def index
	  	render :layout => 'application-admin'
  end

  def about
  end

  private
    # Confirms a logged-in user.
    def logged_in_user
      unless logged_in?            # If not logged_in back to admin login page and can not open all action in admin controller
        #flash[:danger] = "Please log in."
        redirect_to admin_path
      end
    end

end
