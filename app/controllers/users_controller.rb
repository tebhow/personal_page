class UsersController < ApplicationController
  
  def show
      @user = User.find(params[:id])                                    # Get user by id =1 for update and edit form
      @users = User.all                                                 # Get database record to sent to layout about section
      render :layout => 'application-admin', locals: { users: @users }  # push users database record to layout about section
  end

  def about
      #@user = User.find(1)                                              # Get user by id =1 for update and edit form
      #@users = User.all                                                 # Get database record to sent to layout about section
      #render :layout => 'application-admin', locals: { users: @users }  # push users database record to layout about section
  end

  def edit
      @user = User.find(params[:id])                                    # Get user by id =1 for update and edit form
      @users = User.all                                                 # Get database record to sent to layout about section
      render :layout => 'application-admin', locals: { users: @users }  # push users database record to layout about section
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      # Handle a succesful update
      #flash[:success] = "Profile updated"
      redirect_to :back
    else
      redirect_to root_path
    end
  end

  def create
    @user = User.new(user_params)
    if @user.save
      #flash[:success] = "Welcome to the Sample App!"
      redirect_to @user
    else
      render 'new'
    end
  end

  private

    def user_params
      params.require(:user).permit(:fullname)
    end

end
