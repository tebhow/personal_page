class SessionsController < ApplicationController

  def new
      if logged_in?
        redirect_to user        # Reroute if logged_in can not going to admin login page
      else 
        render :layout => 'login-page'
      end
  end

  def create
    user = User.find_by(name: params[:session][:name])
    if user && user.authenticate(params[:session][:password])
      log_in user
      redirect_to user
    else
      # flash[:danger] = 'Invalid email/password combination' # Not quite right! Create an error message.
      redirect_to :back
    end
  end

  def destroy
    log_out if logged_in?
    redirect_to admin_path
  end

end
